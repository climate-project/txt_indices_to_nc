from netCDF4 import Dataset
import numpy as np


def create_nc(dest_path, filename, data_arr, lon_arr, lat_arr, time_arr):
    nc = Dataset(f"{dest_path}/{filename}.nc", "w", format="NETCDF4")
    lat = nc.createDimension("lat", len(lat_arr))
    lon = nc.createDimension("lon", len(lon_arr))
    time = nc.createDimension("time", len(time_arr))

    lats = nc.createVariable("lat", "f8", ("lat", ))
    lons = nc.createVariable("lon", "f8", ("lon", ))
    times = nc.createVariable("time", "i8", ("time", ))
    data = nc.createVariable("Ann", "f8", ("time", "lat", "lon"))
    
    lats[:] = lat_arr
    lons[:] = lon_arr
    times[:] = time_arr
    data[:] = data_arr

    nc.close()
    
    print(f"{dest_path}/{filename}.nc")