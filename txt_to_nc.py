import os
import numpy as np
import pandas as pd

from netCDF4 import Dataset
from create_netcdf import create_nc


data_set = "mpi"
dest_path = f"./nc_output/{data_set}/"
raw_txt_path = f"./txt_indices/{data_set}/"
ref_nc_file = "./nc_ref/MPI47_temp_pr_day.196912.nc"


ref_nc = Dataset(ref_nc_file)
ref_lon = np.array(ref_nc['xlon'][0])
ref_lat = np.array(ref_nc['xlat'][:, 0])

raw_files = os.listdir(raw_txt_path)


for raw_file in raw_files:
    df = pd.read_csv(f"{raw_txt_path}/{raw_file}", sep="\t", header=None).rename(columns={0: 'year'})

    raw_data = []
    for i in range(len(df)):
        raw_data.append(np.array(df.iloc[i, 1:]).reshape(len(ref_lat), len(ref_lon)))
    raw_data = np.array(raw_data, dtype=np.float)

    raw_year = np.array(df['year'])

    create_nc(dest_path, raw_file[:-4], raw_data, ref_lon, ref_lat, raw_year)
