# TXT indices file to NC

แปลงข้อมูลจากไฟล์ txt ไปเป็น nc

## structure
```
/nc_output (เก็บ nc)
|-ecearth
|-hadgem2
|-mpi


/nc_ref (เก็บไฟล์ nc - จากข้อมูลดิบ สำหรับใช้เป็นพิกัดเวลาแปลงไฟล์)
|- *.nc

/txt_indices (ข้อมูล index เป็นไฟล์ txt)

```