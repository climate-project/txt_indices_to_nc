import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap, shiftgrid
import time
from matplotlib import cm

def plot_map(data,
             lons,
             lats,
             significance,
             color_map="RdYlBu_r",
             parallel_s=10,
             meridian_s=10,
             draw_graticule=False,
             save_file=False,
             figsize=(12, 8),
             dpi=200,
             crop=False):
    fig = plt.figure(figsize=figsize, dpi=dpi)

    # map margin
    horizontal_margin = np.abs(lons[0] - lons[1]) * 2
    vertical_margin = np.abs(lats[0] - lats[1]) * 2

    # create map
    if crop:
        m = Basemap(projection="cyl",
                    resolution='i',
                    area_thresh=5000,
                    llcrnrlon=np.min(lons) - horizontal_margin,
                    llcrnrlat=np.min(lats) - vertical_margin,
                    urcrnrlon=np.max(lons) + horizontal_margin,
                    urcrnrlat=np.max(lats) + vertical_margin)
    else:
        m = Basemap(
            projection="cyl",
            resolution='i',
            area_thresh=5000
        )

    if draw_graticule:
        parallels = np.arange(-90, 90 + parallel_s, parallel_s)
        meridians = np.arange(-180, 180 + meridian_s, meridian_s)
        m.drawparallels(parallels, labels=[1, 0, 0, 0], color="#aeaeae", linewidth=0.1, size=5)
        m.drawmeridians(meridians, labels=[1, 0, 0, 1], color="#aeaeae", linewidth=0.1, size=5)

    m.drawcountries(linewidth=0.1)
    m.drawcoastlines(linewidth=0.1)

    # shift lons and lats by step/2
    shift_lons = -np.abs(lons[0] - lons[1]) / 2
    shift_lats = -np.abs(lats[0] - lats[1]) / 2
    x, y = m(*np.meshgrid(lons + shift_lons, lats + shift_lats))

    # colormap setting
    color_res = 32
    ticks = np.linspace(np.nanmin(data), np.nanmax(data), int(color_res / 2) + 1)
    colormap = cm.get_cmap(color_map, color_res)

    # plot grid
    color = m.pcolor(x,
                     y,
                     np.round(data.squeeze(), 2),
                     cmap=colormap,
                     edgecolors="k",
                     linewidth=0.01,
                     vmin=np.nanmin(data),
                     vmax=np.nanmax(data))

    # plot colorbar
    color_bar = m.colorbar(color, location="bottom", pad="5%", ticks=np.round(ticks, 2))
    color_bar.ax.tick_params(labelsize=6)

    # plot trend significance
    if np.array(significance).shape[0] > 0:
        for row in range(len(lats)):
            for col in range(len(lons)):
                if significance[row][col] == 1:
                    plt.plot(lons[col], lats[row], marker='+', markersize=0.1, color='k', linewidth=0.01)
                elif significance[row][col] == -1:
                    plt.plot(lons[col], lats[row], marker="|", markersize=0.1, color='k', linewidth=0.01)

    if save_file:
        plt.savefig(f"result_{int(time.time())}.png", dpi=dpi * 5, bbox_inches='tight', pad_inches=0.2)

    # plt.show()